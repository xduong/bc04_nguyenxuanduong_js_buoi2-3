// Bài 1: Tính tiền lương nhân viên
document.getElementById("tinhLuong").onclick = function () {
  // alert(123)

  var luong1Ngay = document.getElementById("luong1Ngay").value * 1;
  var soNgayLam = document.getElementById("soNgayLam").value * 1;

  var tienLuong = luong1Ngay * soNgayLam;
  document.getElementById("tienLuong").innerHTML =
    tienLuong.toLocaleString() + " vnd";
};

// Bài 2: Tính giá trị trung bình

document.getElementById("tinhTrungBinh").onclick = function () {
  // alert(123)

  var soThu1 = document.getElementById("soThu1").value * 1;
  var soThu2 = document.getElementById("soThu2").value * 1;
  var soThu3 = document.getElementById("soThu3").value * 1;
  var soThu4 = document.getElementById("soThu4").value * 1;
  var soThu5 = document.getElementById("soThu5").value * 1;

  var trungBinh = (soThu1 + soThu2 + soThu3 + soThu4 + soThu5) / 5;

  document.getElementById("giaTriTrungBinh").innerHTML = trungBinh;
};

// Bài 3: Quy đổi tiền
document.getElementById("doiTien").onclick = function () {
  //   alert(33);

  var tienUSD = document.getElementById("tienUSD").value * 1;

  var tienVND = tienUSD * 23500;

  document.getElementById("tienVND").innerHTML =
    "$ " + tienUSD + " = " + tienVND.toLocaleString() + " vnd";
};

//Bài 4: Tính diện tích, chu vi hình chữ nhật

// Tính diệ tích
document.getElementById("tinhDienTich").onclick = function () {
  var chieuDai = document.getElementById("chieuDai").value * 1;
  var chieuRong = document.getElementById("chieuRong").value * 1;

  var dienTich = chieuDai * chieuRong;

  document.getElementById("dienTichVaChuvi").innerHTML =
    "Diện tích: " + dienTich;
};

// Tính Chu vi
document.getElementById("tinhChuvi").onclick = function () {
  var chieuDai = document.getElementById("chieuDai").value * 1;
  var chieuRong = document.getElementById("chieuRong").value * 1;

  var chuVi = (chieuDai + chieuRong) * 2;

  document.getElementById("dienTichVaChuvi").innerHTML = "Chu vi:" + chuVi;
};

// Tính diện tích và chu vi
document.getElementById("tinhDienTichVaChuvi").onclick = function () {
  //   alert(33);

  var chieuDai = document.getElementById("chieuDai").value * 1;
  var chieuRong = document.getElementById("chieuRong").value * 1;

  var dienTich = chieuDai * chieuRong;
  var chuVi = (chieuDai + chieuRong) * 2;

  document.getElementById("dienTichVaChuvi").innerHTML =
    "Diện tích: " + dienTich + "; Chu vi:" + chuVi;
};

//Bài 5: Tính tổng 2 ký số

document.getElementById("tinhTongHaiKySo").onclick = function () {
  //   alert(33);

  var soHaiChuSo = document.getElementById("soHaiChuSo").value * 1;

  var soHangChuc = soHaiChuSo % 10;
  var soHangDonvi = Math.floor(soHaiChuSo / 10);

  document.getElementById("tongHaiKySo").innerHTML = soHangChuc + soHangDonvi;
};
